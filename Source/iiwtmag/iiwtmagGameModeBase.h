// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "iiwtmagGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class IIWTMAG_API AiiwtmagGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
